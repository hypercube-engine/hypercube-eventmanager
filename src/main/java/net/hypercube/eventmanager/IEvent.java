package net.hypercube.eventmanager;

import java.util.Optional;

/**
 * This might look like a Functional Interface, but it isn't. There might be
 * more methods in a future version.
 * 
 * @author Christian Danscheid
 *
 * @param <T> The type of the event data
 */
public interface IEvent<T> {
	Optional<T> getData();
}

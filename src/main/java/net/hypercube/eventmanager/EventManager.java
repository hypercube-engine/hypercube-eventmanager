package net.hypercube.eventmanager;

import java.util.Queue;
import java.util.function.Consumer;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.collections4.queue.CircularFifoQueue;

/**
 * The EventManager handles {@link IEvent event} subscriptions, queueing of
 * {@link IEvent events} and dispatching events upon request.
 * 
 * @author Christian Danscheid
 *
 */
public class EventManager {
	private static final int DEFAULT_EVENT_QUEUE_CAPACITY = 128;

	private final Queue<IEvent<?>> eventQueue;
	private final MultiValuedMap<Class<? extends IEvent<?>>, Consumer<? extends IEvent<?>>> subscribers;

	{
		subscribers = new HashSetValuedHashMap<>();
	}
	
	public EventManager(final int queueCapacity) {
		eventQueue = new CircularFifoQueue<>(queueCapacity);
	}
	
	public EventManager() {
		eventQueue = new CircularFifoQueue<>(DEFAULT_EVENT_QUEUE_CAPACITY);
	}

	/**
	 * Add a new event to the queue.
	 * 
	 * @param event The {@link IEvent event} to enqueue
	 */
	public void enqueue(IEvent<?> event) {
		eventQueue.offer(event);
	}
	
	/**
	 * Add a new event to the queue and dispatch all currently enqueued events.
	 * 
	 * @param event The {@link IEvent event} to enqueue
	 */
	public void enqueueAndDispatch(IEvent<?> event) {
		enqueue(event);
		dispatchAll();
	}

	/**
	 * Subscribe for a certain type of {@link IEvent event}.
	 * 
	 * @param eventClass   The {@link Class} of the {@link IEvent event} to listen
	 *                     to.
	 * @param eventHandler The {@link Consumer} handling the event upon dispatch.
	 */
	public <T extends IEvent<?>> void subscribe(Class<T> eventClass, Consumer<T> eventHandler) {
		subscribers.put(eventClass, eventHandler);
	}

	/**
	 * Unsubscribe from a certain type of {@link IEvent event}.
	 * 
	 * @param eventClass   The {@link Class} of the {@link IEvent event} to
	 *                     unsubscribe from.
	 * @param eventHandler The {@link Consumer} to be unsubscribed.
	 */
	public <T extends IEvent<?>> void unsubscribe(Class<T> eventClass, Consumer<T> eventHandler) {
		subscribers.removeMapping(eventClass, eventHandler);
	}

	/**
	 * Dispatch all currently {@link #enqueue(IEvent) enqueued} {@link IEvent
	 * events}.
	 */
	@SuppressWarnings("unchecked")
	public <T extends IEvent<?>> void dispatchAll() {
		while (eventQueue.size() > 0) {
			final var event = eventQueue.poll();
			final var eventClass = event.getClass();
			subscribers.entries().stream().filter((entry) -> entry.getKey().isAssignableFrom(eventClass))
					.forEach((entry) -> ((Consumer<T>) entry.getValue()).accept((T) event));
		}
	}
}
